# appimageToLauncher

**The contents**
* ./bin : executable script are inside this directory.
     * Do not modify the script.
* ./opt : appimages are inside.
     * Just drag and drop yours inside and delete the exemple here.
* ./share/applications : Shortcuts are inside. 
     * Create your own using the exemple and replace the  /home/user in the paths.
* ./share/icons : Add your icons here.
     * You can download it using Qwant, Google or any search engine or find on the official website or the git projet of the software.

**HOW ?**
* **Be carefull** when you execute commands (and modify your .local). If you don't know what you're doing, **do not hesitate to contact someone who can explain to you**. He will be happy to teach you.
* Copy all of this directories and contents inside the .local ($HOME/.local or ~/.local) directory inside your home directory (<code>$ echo $HOME</code> in a terminal). If the .local not exist, create it.
* Looks if bin path is present with an <code>$ echo $PATH</code> inside a terminal
     * If not add to .profil file (create it if necessary) <code>$ export PATH="~/.local/bin:$PATH"</code> for bash, <code>$ setenv PATH="~/.local/bin:$PATH"</code> for tcsh.
* Put all your appimages inside opt directory
* Create a new desktop launcher inside share/applications (copy the example) and replace /home/user/.local next Exec and Icon by your home directory.
* Delete the exemple files : share/application/My-app.desktop  and opt/my-app-1.1.0-x86_64.AppImage

**Todo list (for this git) :**
* Creat a script detecting new appimages. Once detected, ask for an icon and creat the desktop launcher.
